# Git Projects downloader

This C programm intends to ease my life by offering me to download quickly an high amount of projects by asked me which projects it should clone each time.

## Compiling
`gcc make` must be installed on the target system.

```sh
	make
```

## Installing
```sh
	sudo make install
```

## Configuration
Please create a file named `config.gitdown` with the following syntax:

`project name|storage_dir|source_directory|destination_directory`

You can then run the command `gitprojectdownloader` inside of the directory that contains this file.

## Uninstall
```sh
	sudo make uninstall
```

## License
This project is licensed under the MIT License

## Author
Pierre HUBERT, 2018
