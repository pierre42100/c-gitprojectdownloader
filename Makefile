CC=gcc
CFLAGS= -Wall
LIBS=
OBJ=main.o

all: clean gitprojectdownloader

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

gitprojectdownloader: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f gitprojectdownloader $(OBJ)

run: all
	./gitprojectdownloader

install: all
	cp ./gitprojectdownloader /usr/bin/

uninstall:
	rm -f /usr/bin/gitprojectdownloader