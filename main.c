/**
 * Git projects downloader
 *
 * This script migth be usefull to quickly clone for backup
 * purpose a large amount of repositories
 *
 * @author Pierre HUBERT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>

/**
 * The name of the configuration file
 */
#define FILE_CONF "config.gitdown"

/**
 * The maximum length of the command to execute
 */
#define COMMAND_MAX_LENGTH 500


typedef struct project_t {
	char *name;
	char *storage_dir;
	char *src_repo;
	char *dest_repo;
	struct project_t *previous_project;
} project_t;

/**
 * Warning ! This macro declares a ret int into the scop of the block that call it
 */
#define EXECUTE_WHILE_NOT_WORK(cmd, fail_msg) int ret = -1; do {ret = system_exec(cmd);} while(ret != 0 && ask_user_choice(fail_msg));

#define FATAL_ERROR(msg) { fprintf(stderr, "Fatal error: %s\n", msg); exit(EXIT_FAILURE); }
#define MALLOC_OR_FATAL(target, size){target = malloc(size); if(target == NULL) FATAL_ERROR("Could not allocate memory!"); }

static project_t *last_project = NULL;

/**
 * Show application usage
 */
static void show_usage(){
	printf("gitprojectdownoader - A tool to easily download a large amount of repositories\n"
			"Command usage: \n"
			"\t download Download the repositories\n"
			"\t push     Push the repositories to destination repo\n"
			"\t clean    Remove all the swap directories\n"
			"\t help     Show this help\n"
			"\n");
}


/**
 * Read a string until line break or | character
 *
 * @param dest The destination string
 * @param src The source string to process
 * @return New position to start of string
 */
static char * read_next_conf_string(char **dest, const char *src){

	char *stop = strchr(src, '|');

	if(stop == NULL)
		stop = strchr(src, '\n');
	if(stop == NULL)
		stop = strchr(src, '\0');

	if(stop == NULL)
		FATAL_ERROR("Could not find stop point!");

	//Allocate appropriate memory to store string
	MALLOC_OR_FATAL(*dest, stop-src+1);

	//Copy string into target
	strncpy(*dest, src, stop-src);
	*(*dest+(stop-src+1)) = '\0';

	return stop+1;
}

/**
 * Load application configuration
 *
 * This function migth make the program crash in case
 * of failure
 */
static void load_config(){
	//Open configuration file
	FILE *fconf = fopen(FILE_CONF,"r");
	if(fconf == NULL)
		FATAL_ERROR("Could not open configuration file "FILE_CONF" !");

	//Load the list of configurations
	char line[500];
	while(fgets(line, 500-1, fconf) != NULL){

		project_t *curr_project = NULL;
		MALLOC_OR_FATAL(curr_project, sizeof(project_t));

		//Process project
		char *curr_pos = read_next_conf_string(&curr_project->name, line);
		curr_pos = read_next_conf_string(&curr_project->storage_dir, curr_pos);
		curr_pos = read_next_conf_string(&curr_project->src_repo, curr_pos);
		curr_pos = read_next_conf_string(&curr_project->dest_repo, curr_pos);
		
		//Add it to the list
		curr_project->previous_project = last_project;
		last_project = curr_project;
	}

	//Close configuration file
	fclose(fconf);
}

/**
 * Ask user choice
 *
 * @param msg The message to show to the user
 * @return 0 = yes / other = 0
 */
static int ask_user_choice(const char *msg){
	char answer = '\0';
	do {
		printf("%s [y/n] ", msg);
		answer = getchar();

		//Clean input buffer
		while(answer != '\n' && getchar() != '\n');

	} while(answer != 'y' && answer != 'n');

	return answer == 'y';
}

/**
 * Check out whether a directory exists or not
 *
 * @param path The path of the directory to check
 * @return 0 if the directory does not exists or can not
 * be accessed / another value else
 */
static int directory_exists(const char *path) {

	errno = 0;
	DIR *dir = opendir(path);

	//Check if directory could be opened
	if(dir != NULL){
		closedir(dir);
		return 1;
	}

	return 0; //Directory could not be opened or does not exists

}

/**
 * Execute a command external to the programm and return
 * its result
 *
 * @param cmd The command to execute
 * @return int The return code of the function
 */
static int system_exec(const char *cmd){
	fprintf(stdout, "Executing: %s\n", cmd);
	return system(cmd);
}

/**
 * Change the origin url of a repository
 *
 * @param *project The target project to update
 * @param *new_origin The new origin to apply to the project
 */
static void change_repo_origin_url(const project_t *project, const char *new_origin){

	char command[COMMAND_MAX_LENGTH];

	//Update remote url of repository
	sprintf(command, 
		"git --git-dir=%s/.git remote set-url origin %s", 
		project->storage_dir, 
		new_origin);
	system_exec(command);

}

/**
 * Clone a project using given information
 *
 * @param project_t *project Information about the
 * project to clone
 */
static void clone_project(const project_t *project){

	char command[COMMAND_MAX_LENGTH];
	sprintf(command, "git clone %s %s", project->src_repo, project->storage_dir);
	
	EXECUTE_WHILE_NOT_WORK(command, "Failed to clone repository. Try again?");

	//Check if we failed to clone repository
	if(ret != 0)
		return;

}

/**
 * Pull (update) an existing project from the source repository
 *
 * @param project_t *project Information about the
 * project to clone
 */
static void pull_project(const project_t *project){

	char command[COMMAND_MAX_LENGTH];

	//Make sure the origin point to the source repository
	change_repo_origin_url(project, project->src_repo);

	sprintf(command, "cd %s && git pull", project->storage_dir);
	
	EXECUTE_WHILE_NOT_WORK(command, "Failed to pull repository. Try again?");

}

/**
 * Perform download of repositories
 */
static void download() {

	load_config();

	//Now we process the entire list of project to ask the user if he wants to download them
	project_t *curr_project = last_project;
	while(curr_project != NULL){
		
		//First check if the repository as already been cloned
		if(directory_exists(curr_project->storage_dir))

			//We consider by default that the user wants to pull all the projects
			//He has already cloned.
			pull_project(curr_project);

		else {

			//Clone current project if requested
			char question[COMMAND_MAX_LENGTH];
			sprintf(question, "Would you like to clone %s ?", curr_project->name);
			if(ask_user_choice(question))
				clone_project(curr_project);

		}

		curr_project = curr_project->previous_project;
	}

}

/**
 * Push the project to the local repository
 *
 * @param *project The project to push
 */
static void push_project(const project_t *project){

	char command[COMMAND_MAX_LENGTH];

	//Push project to destination repository
	printf("\n\nPushing project %s to destination repository\n", project->name);

	//Update remote url of repository
	change_repo_origin_url(project, project->dest_repo);

	sprintf(command, "git --git-dir=%s/.git push -u origin", project->storage_dir);
	EXECUTE_WHILE_NOT_WORK(command, "Failed to push repo. Would you like to try again ?");
}

/**
 * Push the repositories to their destination
 */
static void push(){
	printf("Push repositories to destination repositories\n");

	load_config();

	//Process the list of repositories
	project_t *curr_project = last_project;
	while(curr_project != NULL){

		//Push the project if available locally
		//Check if directory exists
		if(directory_exists(curr_project->storage_dir))
			push_project(curr_project);
		curr_project = curr_project->previous_project;
	}

}

/**
 * Removes the swap directory of a project
 *
 * @param *project Information about the target project
 */
static void delete_swap_directory(const project_t *project){
	char command[COMMAND_MAX_LENGTH];

	sprintf(command, "rm -rf %s", project->storage_dir);
	EXECUTE_WHILE_NOT_WORK(command, "Failed to deleted storage directory of the current project. Would you like to try again ?");
}

/**
 * Clean all swap directories
 */
static void clean(){
	load_config();

	//Process the list of repositories
	project_t *curr_project = last_project;
	while(curr_project != NULL){

		//Push the project if available locally
		//Check if directory exists
		if(directory_exists(curr_project->storage_dir))
			delete_swap_directory(curr_project);
		curr_project = curr_project->previous_project;
	}
}

int main(int argc, char **argv){
	printf("gitprojectdownoader - (c) Pierre HUBERT 2018\n"
		   "Project licensed under the MIT License.\n\n\n");

	//Check if the request was not specified
	if(argc < 2)
		show_usage();

	//Show help
	else if(strcmp(argv[1], "help") == 0)
		show_usage();

	//Download the list of repositories
	else if(strcmp(argv[1], "download") == 0)
		download();

	//Push downloaded repositories
	else if(strcmp(argv[1], "push") == 0)
		push();

	//Clean all repositories
	else if(strcmp(argv[1], "clean") == 0)
		clean();

	//Command not understood
	else
		FATAL_ERROR("Could not understand your request!");

	return EXIT_SUCCESS;
}
